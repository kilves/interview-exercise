import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {UserListComponent} from "./component/user-list/user-list.component";
import {UserComponent} from "./component/user/user.component";
import {NewUserComponent} from "./component/new-user/new-user.component";

const routes: Routes = [
    {
        path: "",
        pathMatch: "full",
        redirectTo: "/users"
    },
    {
        path: "users/new",
        component: NewUserComponent
    },
    {
        path: "users",
        component: UserListComponent,
    },
    {
        path: "users/:id",
        component: UserComponent
    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
