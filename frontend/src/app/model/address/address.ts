import {GeoPoint} from "../geoPoint";

export interface Address {
    id: number;
    street?: string;
    suite?: string;
    city?: string;
    zipcode?: string;
    geo?: GeoPoint;
}
