import {CreateUser} from "./createUser";

export interface ListUser extends CreateUser {
    id: number;
}
