import {Address} from "../address/address";
import {Company} from "../company/company";

export interface CreateUser {
    name?: string;
    username: string;
    email?: string;
    phone?: string;
    website?: string;
    company?: Company,
    address?: Address
}
