import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";

import {AppRoutingModule} from "./app-routing.module";
import {AppComponent} from "./app.component";
import {UserListComponent} from "./component/user-list/user-list.component";
import {HttpClientModule} from "@angular/common/http";
import { UserComponent } from './component/user/user.component';
import {FormsModule} from "@angular/forms";
import { NewUserComponent } from './component/new-user/new-user.component';
import { UserFormComponent } from './component/user-form/user-form.component';

@NgModule({
    declarations: [
        AppComponent,
        UserListComponent,
        UserComponent,
        NewUserComponent,
        UserFormComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
