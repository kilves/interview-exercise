import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {lastValueFrom} from "rxjs";
import {ListUser} from "../model/user/listUser";
import {CreateUser} from "../model/user/createUser";

@Injectable({
    providedIn: "root"
})
export class UserService {

    apiUrl = "/api/users";

    constructor(private http: HttpClient) {}

    listAll() {
        return lastValueFrom(
            this.http.get<ListUser[]>(this.apiUrl)
        );
    }

    async delete(user: ListUser) {
        return lastValueFrom(
            this.http.delete(`${this.apiUrl}/${user.id}`)
        );
    }

    get(id: number) {
        return lastValueFrom(
            this.http.get<ListUser>(`${this.apiUrl}/${id}`)
        );
    }

    save(user: ListUser) {
        return lastValueFrom(
            this.http.put(`${this.apiUrl}/${user.id}`, user)
        );
    }

    create(user: CreateUser) {
        return lastValueFrom(
            this.http.post(`${this.apiUrl}`, user)
        );
    }
}
