import {Component, OnInit} from "@angular/core";
import {ListUser} from "../../model/user/listUser";
import {UserService} from "../../service/user.service";

@Component({
    selector: "app-user-list",
    templateUrl: "./user-list.component.html",
    styleUrls: ["./user-list.component.css"]
})
export class UserListComponent implements OnInit {
    users: ListUser[] = [];
    constructor(private userService: UserService) {
    }

    async ngOnInit() {
        this.users = await this.userService.listAll();
    }

    async delete(user: ListUser) {
        const res = confirm(`Are you sure you want to delete ${user.name}?`);
        if (res) {
            await this.userService.delete(user);
        }
        this.users = await this.userService.listAll();
    }
}
