import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {UserService} from "../../service/user.service";
import {ListUser} from "../../model/user/listUser";
import {CreateUser} from "../../model/user/createUser";

@Component({
    selector: 'app-user',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

    user: ListUser | null = null;

    constructor(private route: ActivatedRoute,
                private userService: UserService) {
    }

    ngOnInit(): void {
        this.route.params.subscribe(async params => {
            this.user = await this.userService.get(+params["id"]);
        })
    }

    async saved(user: CreateUser) {
        if (!this.user) {
            return;
        }
        await this.userService.save({
            id: this.user.id,
            ...user
        });
    }
}
