import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ListUser} from "../../model/user/listUser";
import {CreateUser} from "../../model/user/createUser";

@Component({
    selector: 'app-user-form',
    templateUrl: './user-form.component.html',
    styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {

    saved = false;

    @Input() user: CreateUser | null = null;
    @Output() onSave = new EventEmitter<CreateUser>();
    constructor() {
    }

    ngOnInit(): void {
    }

    submit() {
        if (!this.user) {
            return;
        }
        this.onSave.emit(this.user)
        this.saved = true;
        setTimeout(() => {
            this.saved = false;
        }, 2000);
    }
}
