import {Component} from "@angular/core";
import {CreateUser} from "../../model/user/createUser";
import {Router} from "@angular/router";
import {UserService} from "../../service/user.service";

@Component({
    selector: "app-new-user",
    templateUrl: "./new-user.component.html",
    styleUrls: ["./new-user.component.css"]
})
export class NewUserComponent {

    user: CreateUser = {
        name: "",
        username: "",
        email: "",
        phone: "",
        website: "",
    }

    constructor(private router: Router,
                private userService: UserService) {
    }

    async createUser(user: CreateUser) {
        await this.userService.create(user);
        return this.router.navigate(["/users"]);
    }
}
