export interface CompanyDto {
    id: number;
    name: string;
    catchPhrase?: string;
    bs?: string;
}