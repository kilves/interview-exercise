import {GeoPointDto} from "./geoPoint.ts";

export interface AddressDto {
    id: number;
    street?: string;
    suite?: string;
    city?: string;
    zipcode?: string;
    geo?: GeoPointDto;
}