import {AppUser} from "../model/appUser.ts";
import {CompanyDto} from "./company.ts";
import {AddressDto} from "./address.ts";

export interface CreateUserDto {
    name?: string;
    username: string;
    email?: string;
    phone?: string;
    website?: string;
}

export interface EditUserDto extends CreateUserDto {
    id: number;
}

export interface ListUserDto {
    id: number;
    name?: string;
    username: string;
    email?: string;
    phone?: string;
    website?: string;
    company?: CompanyDto,
    address?: AddressDto
}
