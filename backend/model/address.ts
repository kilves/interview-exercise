import {BaseEntity, Column, Entity, Point, PrimaryGeneratedColumn} from "typeorm";
import {Convertable} from "./convertable.ts";
import {AddressDto} from "../dto/address.ts";

@Entity()
export class Address extends BaseEntity implements Convertable {
    @PrimaryGeneratedColumn()
    id: number;

    @Column("text")
    street: string | null;

    @Column("text")
    suite: string | null;

    @Column("text")
    city: string | null;

    @Column("text")
    zipcode: string | null;

    @Column("point")
    geo: Point | null;

    convertToDto(): AddressDto {
        return {
            id: this.id,
            city: this.city || undefined,
            geo: this.geo ? {
                lat: this.geo.coordinates[0],
                lon: this.geo.coordinates[1]
            } : undefined,
            street: this.street || undefined,
            suite: this.suite || undefined,
            zipcode: this.zipcode || undefined
        }
    }
}

/*
    id serial primary key,
    street text,
    suite text,
    city text,
    zipcode text,
    geo_id int references geo(id)
 */