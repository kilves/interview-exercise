import {BaseEntity, Column, Entity, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {AppUser} from "./appUser.ts";
import {Convertable} from "./convertable.ts";
import {CompanyDto} from "../dto/company.ts";

@Entity()
export class Company extends BaseEntity implements Convertable {
    @PrimaryGeneratedColumn()
    id: number;

    @Column("text", {
        nullable: false
    })
    name: string;

    @Column("text")
    catchPhrase: string | null;

    @Column("text")
    bs: string | null;

    @OneToMany(type => AppUser, user => user.company, {
        lazy: true
    })
    users: Promise<AppUser[]>;

    convertToDto(): CompanyDto {
        return {
            id: 0,
            bs: this.bs || undefined,
            catchPhrase: this.catchPhrase || undefined,
            name: this.name
        }
    }
}

/*
    id serial primary key,
    name text,
    catchPhrase text,
    bs text
 */