export interface Convertable {
    convertToDto(): any;
}