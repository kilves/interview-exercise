import {BaseEntity, Column, Entity, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {Company} from "./company.ts";
import {Address} from "./address.ts";
import {ListUserDto} from "../dto/user.ts";
import {Convertable} from "./convertable.ts";

@Entity()
export class AppUser extends BaseEntity implements Convertable {
    @PrimaryGeneratedColumn()
    id: number;

    @Column("text")
    name: string | null;

    @Column("text", {
        nullable: false
    })
    username: string;

    @Column("text")
    email: string | null;

    @Column("text")
    phone: string | null;

    @Column("text")
    website: string | null;

    @ManyToOne(type => Company, company => company.users, {
        eager: true
    })
    company: Company | null;

    @ManyToOne(type => Company, company => company.users, {
        eager: true
    })
    address: Address | null;

    convertToDto(): ListUserDto {
        return {
            address: this.address?.convertToDto(),
            company: this.company?.convertToDto(),
            email: this.email || undefined,
            id: this.id,
            name: this.name || undefined,
            phone: this.phone || undefined,
            username: this.username,
            website: this.website || undefined
        }
    }
}