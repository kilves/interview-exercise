import Koa from "koa";
import Router from "@koa/router";
import bodyParser from "koa-body";
import api from "./api/api";

const app = new Koa();
const router = new Router();
await import("./dataSource.ts");


router.use("/api", api.routes());

app
    .use(bodyParser())
    .use(router.routes())
    .use(router.allowedMethods());

app.listen(3001);