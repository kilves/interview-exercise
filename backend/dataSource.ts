import {DataSource} from "typeorm";
import {SnakeNamingStrategy} from "typeorm-naming-strategies";

const AppDataSource = new DataSource({
    type: "postgres",
    host: process.env.DB_HOST,
    port: Number.parseInt(process.env.DB_PORT || "5432"),
    username: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME,
    entities: ["model/*.ts"],
    namingStrategy: new SnakeNamingStrategy(),
});

await AppDataSource.initialize();

export {
    AppDataSource
};