import Router from "@koa/router";
import {AppUser} from "../model/appUser.ts";
import {CreateUserDto, EditUserDto} from "../dto/user.ts";

const router = new Router();

router.get("/", async ctx => {
    const users = await AppUser.find();
    ctx.response.body =  users.map(u => u.convertToDto());
    ctx.response.status = 200;
});

router.get("/:id", async ctx => {
    const user = await AppUser.findOneBy({
        id: +ctx.params.id
    });
    if (user === null) {
        ctx.response.body = {};
        ctx.response.status = 404;
        return;
    }
    ctx.response.body = user.convertToDto();
});

router.delete("/:id", async ctx => {
    const user = await AppUser.findOneBy({
        id: +ctx.params.id
    });

    if (!user) {
        ctx.response.status = 404;
        ctx.response.body = {};
        return;
    }
    user?.remove();
    ctx.response.body = {};
});

router.put("/:id", async ctx => {
    const body = ctx.request.body as EditUserDto;
    const user = await AppUser.findOneBy({
        id: +ctx.params.id
    })
    if (!user) {
        ctx.response.body = {}
        ctx.response.status = 404;
        return;
    }
    user.name = body.name || null;
    user.username = body.username;
    user.email = body.email || null;
    user.phone = body.phone || null;
    user.website = body.website || null;
    await user.save();
    ctx.response.body = {}
    ctx.response.status = 200;
});

router.post("/", async ctx => {
    const body = ctx.request.body as CreateUserDto;
    const user = AppUser.create({
        name: body.name,
        username: body.username,
        email: body.email,
        phone: body.phone,
        website: body.website
    });
    await user.save();
    ctx.response.body = {};
    ctx.response.status = 200;
});

export default router;