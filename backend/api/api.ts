import Router from "@koa/router";
import user from "./user.ts";

const router = new Router();

router.use("/users", user.routes());

export default router;