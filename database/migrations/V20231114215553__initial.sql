create table company(
    id serial primary key,
    name text not null,
    catchPhrase text,
    bs text
);

create table address(
    id serial primary key,
    street text,
    suite text,
    city text,
    zipcode text,
    geo point
);

create table app_user(
    id serial primary key,
    name text,
    username text not null,
    email text,
    phone text,
    website text,
    company_id int references company(id),
    address_id int references address(id)
);
